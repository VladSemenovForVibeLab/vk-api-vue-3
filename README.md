# Приложение для вытаскивания людей из групп ВКонтакте

Это приложение использует Vk Api, Vue 3, Bootstrap и Echarts для вытаскивания информации о людях из групп ВКонтакте.

## Описание

Это приложение разработано для удобного и быстрого получения информации о пользователях, состоящих в группах ВКонтакте. Приложение позволяет выбрать группу, из которой нужно вытянуть информацию о людях, а затем показывает статистику по этим пользователям.

## Установка и запуск

1. Убедитесь, что у вас установлен Node.js.
2. Склонируйте репозиторий на свой компьютер.
3. Откройте терминал и перейдите в папку с проектом.
4. Установите зависимости, выполнив команду `npm i`.
5. Запустите приложение, выполнив команду `npm run dev`.

## Используемые технологии

- **Vue 3**: JavaScript-фреймворк для создания пользовательских интерфейсов.
- **Bootstrap**: Фреймворк для разработки адаптивных и стильных веб-приложений.
- **Echarts**: JavaScript-библиотека для создания интерактивных графиков и диаграмм.

## Автор

Приложение разработано Семеновым Владиславом.